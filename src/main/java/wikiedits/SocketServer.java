package wikiedits;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServer {

	public static void main(String[] args) {
		try {
			ServerSocket ss = new ServerSocket(9000);
			System.out.println("启动服务器....");
			Socket s = ss.accept();
			System.out.println("客户端:" + s.getInetAddress().getLocalHost() + "已连接到服务器");

			BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			// 读取客户端发送来的消息
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
			while (true) {
				bw.write(((int) (Math.random() * 100)) % 10 + "\t");
				bw.write(((int) (Math.random() * 100)) % 10 + "\n");
				bw.flush();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
