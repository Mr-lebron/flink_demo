package wikiedits;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.FoldFunction;
import org.apache.flink.api.common.io.FileInputFormat;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.io.PojoCsvInputFormat;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;
import org.apache.flink.api.java.typeutils.TypeExtractor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.core.fs.Path;
import org.apache.flink.shaded.curator.org.apache.curator.shaded.com.google.common.base.Objects;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.wikiedits.WikipediaEditEvent;
import org.apache.flink.streaming.connectors.wikiedits.WikipediaEditsSource;
import org.apache.flink.util.Collector;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlinkDemoApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(FlinkDemoApplication.class, args);
		hotItemDemo();
		// wordCountDemo();
		// officialDemo();
	}

	/**
	 * 计算热门商品 1.从数据源读取数据 2.过滤掉非点击的数据
	 * 
	 * @throws Exception
	 */
	private static void hotItemDemo() throws Exception {
		// 创建环境
		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		// 设置窗口的时间,由自定义时间确定，而不是程序使用的机器时间
		env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
		// 设置为单个实例处理
		env.setParallelism(1);
		// 设置数据源
		// UserBehavior.csv 的本地文件路径, 在 resources 目录下
		URL fileUrl = FlinkDemoApplication.class.getClassLoader().getResource("item.csv");
		Path filePath = Path.fromLocalFile(new File(fileUrl.toURI()));
		// 抽取 UserBehavior 的 TypeInformation，是一个 PojoTypeInfo
		PojoTypeInfo<UserBehavior> pojoType = (PojoTypeInfo<UserBehavior>) TypeExtractor
				.createTypeInfo(UserBehavior.class);
		// 由于 Java 反射抽取出的字段顺序是不确定的，需要显式指定下文件中字段的顺序
		String[] fieldOrder = new String[] { "userId", "itemId", "categoryId", "behavior", "timestamp" };
		// 创建 PojoCsvInputFormat
		PojoCsvInputFormat<UserBehavior> csvInput = new PojoCsvInputFormat(filePath, pojoType, fieldOrder);

		// 数据处理过程
		WindowedStream<UserBehavior, Tuple, TimeWindow> windowStream = env.createInput(csvInput, pojoType)//
				.filter(new FilterFunction<UserBehavior>() {// 过滤掉非点击数据
					@Override
					public boolean filter(UserBehavior ub) throws Exception {
						return Objects.equal(ub.behavior, "pv");
					}
				}) //
				.assignTimestampsAndWatermarks(new AscendingTimestampExtractor<UserBehavior>() {// 设置自定义时间和waterMark
					@Override
					public long extractAscendingTimestamp(UserBehavior ub) {
						return ub.timestamp * 1000;
					}
				})//
				.keyBy("itemId")// itemId作为key
				.timeWindow(Time.minutes(60), Time.seconds(60));// 窗口时间和滑动窗口时间
		// 窗口数据聚合
		DataStream<ItemViewCount> windowResult = windowStream
				.aggregate(new AggregateFunction<UserBehavior, Long, Long>() {// 计算同一个window的相同itemid的数量
					@Override
					public Long createAccumulator() {
						return 0L;
					}

					@Override
					public Long add(UserBehavior value, Long accumulator) {
						return accumulator + 1;
					}

					@Override
					public Long getResult(Long accumulator) {
						return accumulator;
					}

					@Override
					public Long merge(Long a, Long b) {
						return a + b;
					}
				}, new WindowFunction<Long, ItemViewCount, Tuple, TimeWindow>() {// 对同一个window相同itemid（上面的即结果）进行处理输出自定义结果

					@Override
					public void apply(Tuple key, TimeWindow window, Iterable<Long> input, Collector<ItemViewCount> out)
							throws Exception {
						Long itemId = ((Tuple1<Long>) key).f0;
						Long count = input.iterator().next();
						out.collect(ItemViewCount.of(itemId, window.getEnd(), count));
					}
				});
		// 求商品top
		DataStream<String> topItems = windowResult.keyBy("windowEnd")//
				.process(new KeyedProcessFunction<Tuple, ItemViewCount, String>() {
					private final int topSize = 3;

					// 用于存储商品与点击数的状态，待收齐同一个窗口的数据后，再触发 TopN 计算
					private ListState<ItemViewCount> itemState;

					@Override
					public void onTimer(long timestamp,
							KeyedProcessFunction<Tuple, ItemViewCount, String>.OnTimerContext ctx,
							Collector<String> out) throws Exception {
						// 获取收到的所有商品点击量
						List<ItemViewCount> allItems = new ArrayList<>();
						for (ItemViewCount item : itemState.get()) {
							allItems.add(item);
						}
						// 提前清除状态中的数据，释放空间
						itemState.clear();
						// 按照点击量从大到小排序
						allItems.sort(new Comparator<ItemViewCount>() {
							@Override
							public int compare(ItemViewCount o1, ItemViewCount o2) {
								return (int) (o2.viewCount - o1.viewCount);
							}
						});
						// 将排名信息格式化成 String, 便于打印
						StringBuilder result = new StringBuilder();
						result.append("====================================\n");
						result.append("时间: ").append(new Timestamp(timestamp - 1)).append("\n");
						for (int i = 0; i < topSize; i++) {
							ItemViewCount currentItem = allItems.get(i);
							// No1: 商品ID=12224 浏览量=2413
							result.append("No").append(i).append(":").append("  商品ID=").append(currentItem.itemId)
									.append("  浏览量=").append(currentItem.viewCount).append("\n");
						}
						result.append("====================================\n\n");

						out.collect(result.toString());
					}

					@Override
					public void open(Configuration parameters) throws Exception {
						super.open(parameters);
						// 状态的注册
						ListStateDescriptor<ItemViewCount> itemsStateDesc = new ListStateDescriptor<>("itemState-state",
								ItemViewCount.class);
						itemState = getRuntimeContext().getListState(itemsStateDesc);
					}

					@Override
					public void processElement(ItemViewCount input,
							KeyedProcessFunction<Tuple, ItemViewCount, String>.Context context, Collector<String> out)
							throws Exception {
						// 每条数据都保存到状态中
						itemState.add(input);
						// 注册 windowEnd+1 的 EventTime Timer,
						// 当触发时，说明收齐了属于windowEnd窗口的所有商品数据
						context.timerService().registerEventTimeTimer(input.windowEnd + 1);
					}

				});
		topItems.print();
		env.execute();

	}

	private static void wordCountDemo() throws Exception {
		// 创建环境
		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		// 设置数据源
		DataStream<String> text = env.socketTextStream("localhost", 9000, "\n");
		// 数据源数据处理,将文本串解析成(单词-数量)元组
		DataStream<Tuple2<String, Integer>> wordCounts = text
				.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
					@Override
					public void flatMap(String value, Collector<Tuple2<String, Integer>> out) {
						// System.out.println(value);
						for (String word : value.split("\\s")) {
							out.collect(Tuple2.of(word, 1));
						}
					}
				});
		DataStream<Tuple2<String, Integer>> windowCounts = wordCounts//
				.keyBy(0)// 设置key为元组的第一个字段
				.timeWindow(Time.seconds(5))// 设置时间窗口
				.sum(1);// 对元组的第二个字段做求和操作
		// 打印结果
		windowCounts.print().setParallelism(1);
		// 启动计算
		env.execute("Socket Window WordCount");

	}

	private static void officialDemo() throws Exception {
		StreamExecutionEnvironment see = StreamExecutionEnvironment.getExecutionEnvironment();
		DataStream<WikipediaEditEvent> edits = see.addSource(new WikipediaEditsSource());

		KeyedStream<WikipediaEditEvent, String> keyedEdits = edits.keyBy(new KeySelector<WikipediaEditEvent, String>() {
			@Override
			public String getKey(WikipediaEditEvent event) {
				return event.getUser();
			}
		});
		DataStream<Tuple2<String, Long>> result = keyedEdits.timeWindow(Time.seconds(5)).fold(new Tuple2<>("", 0L),
				new FoldFunction<WikipediaEditEvent, Tuple2<String, Long>>() {
					@Override
					public Tuple2<String, Long> fold(Tuple2<String, Long> acc, WikipediaEditEvent event) {
						acc.f0 = event.getUser();
						acc.f1 += event.getByteDiff();
						return acc;
					}
				});

		result.print();

		see.execute();
	}

	/** 商品点击量(窗口操作的输出类型) */
	public static class ItemViewCount {
		public long itemId; // 商品ID
		public long windowEnd; // 窗口结束时间戳
		public long viewCount; // 商品的点击量

		public static ItemViewCount of(long itemId, long windowEnd, long viewCount) {
			ItemViewCount result = new ItemViewCount();
			result.itemId = itemId;
			result.windowEnd = windowEnd;
			result.viewCount = viewCount;
			return result;
		}
	}

	/** 用户行为数据结构 **/
	public static class UserBehavior {
		public long userId; // 用户ID
		public long itemId; // 商品ID
		public int categoryId; // 商品类目ID
		public String behavior; // 用户行为, 包括("pv", "buy", "cart", "fav")
		public long timestamp; // 行为发生的时间戳，单位秒
	}
}
